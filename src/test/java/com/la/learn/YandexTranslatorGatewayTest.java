package com.la.learn;

import com.la.learn.gateway.YandexTranslatorGateway;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.junit.Assert;
import org.junit.Test;

public class YandexTranslatorGatewayTest {

    private static final String LANG_TRANSLATE = "en-ru";

    @Test
    public void translateTest() throws UnirestException {
        String textForTranslate = "Hello World!";

        YandexTranslatorGateway yandexTranslatorGateway = new YandexTranslatorGateway();
        String translatedText = yandexTranslatorGateway.translate(LANG_TRANSLATE, textForTranslate);

        String expectedValue = "Привет Мир!";
        Assert.assertEquals(expectedValue, translatedText);
    }
}
