package com.la.learn.gateway;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.json.JSONObject;

public class YandexTranslatorGateway {

    private static final String API_KEY = "trnsl.1.1.20130918T211717Z.3ab20d41ced64e5d.224265dad05a5d229373fbeff6090f3c95107941";
    private static final String YANDEX_TRANSLATOR_URL = "https://translate.yandex.net/api/v1.5/tr.json/translate";

    public String translate(String langTranslate, String text) throws UnirestException {
        HttpResponse<JsonNode> jsonResponse = Unirest.get(YANDEX_TRANSLATOR_URL)
                                                     .header("accept", "application/json")
                                                     .queryString("key", API_KEY)
                                                     .queryString("lang", langTranslate)
                                                     .queryString("text", text)
                                                     .asJson();
        JSONObject responseBody = jsonResponse.getBody().getObject();
        return responseBody.getString("text");
    }
}
